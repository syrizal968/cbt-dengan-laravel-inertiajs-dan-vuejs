<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamGroup extends Model
{
    use HasFactory;

    protected $fillable = [
    'exam_id',    
    'exam_session_id',
    'student_id'
];


    /**
     * Get the Exam that owns the ExamGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Exam()
    {
        return $this->belongsTo(Exam::class);
    }

    /**
     * Get the Exam_session that owns the ExamGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Exam_session()
    {
        return $this->belongsTo(ExamSession::class);
    }

    /**
     * Get the Student that owns the ExamGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
