<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'classroom_id', 
        'nisn', 
        'name', 
        'password', 
        'gender'];

    /**
     * Get the Classroom that owns the Student
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Classroom()
    {
        return $this->belongsTo(ClassRoom::class);
    }
}
