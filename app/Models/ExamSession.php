<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamSession extends Model
{
    use HasFactory;


    protected $fillable = [
        'exam_id',
        'title',
        'start_time',
        'end_time',
    ];

    /**
     * Get all of the Exam_group for the ExamSession
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Exam_groups()
    {
        return $this->hasMany(ExamGroup::class);
    }

    /**
     * Get all of the Exam for the ExamSession
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Exam()
    {
        return $this->hasMany(Exam::class);
    }
}
