<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'lesson_id',
        'classroom_id',
        'duration',
        'description',
        'random_question',
        'random_answer',
        'show_answer',
    ];

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    /**
     * Get the Classroom that owns the Exam
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Classroom()
    {
        return $this->belongsTo(ClassRoom::class);
    }

    /**
     * Get all of the Questions for the Exam
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Questions()
    {
        return $this->hasMany(Questions::class)->orderBy('id','DESC');
    }
}
